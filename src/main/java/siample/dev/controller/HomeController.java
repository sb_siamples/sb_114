package siample.dev.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import siample.dev.domain.Story;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String index(Model model, Locale locale) {
		model.addAttribute("titel", "FIFA");
		model.addAttribute("stories", getStories());
		
		System.out.println(String.format("Request received. Language: %s, Country: %s %n",
				                         locale.getLanguage(), locale.getCountry()));
		return "stories";
	}

	private ArrayList<Story> getStories() {
      ArrayList <Story> stories = new ArrayList<Story>();
		
		stories.add(new Story("Der Titel", "Romhányi", "Romhányi József rímhányó híresség volt", new Date()));
		stories.add(new Story("2nd address", "dr dr", "Sas József humorista színházigazgató",    new Date()));
		
		return stories;
	}
}
